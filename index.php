<?php

include("classes/Dbconfig.php");
include("classes/GetProducts.php");
include("classes/DisplayProducts.php");

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--  CSS -->
    <link rel="stylesheet" href="reset.css" />
    <link rel="stylesheet" href="styles.css" />


    <title>ScandiWeb</title>
  </head>
  <body>
    <div class="container">
      <h1>Product List</h1>
      <span class="underline"></span>
      <ul class="items">
        <?php
          //Showing items from all categories
          $products = new DisplayProduct();
          $products-> displayDiscs();
          $products-> displayBooks();
          $products-> displayFurniture();
        ?>
      </ul>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="scripts.js"></script>
  </body>
</html>
