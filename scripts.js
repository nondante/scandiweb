// Hide/show additional fields
var category = document.getElementById("category");

$(".DVD-disc").hide();
$(".Book").hide();
$(".Furniture").hide();


function showFields(){
  if(category.value == "Book"){
    $(".Furniture").hide();
    $(".DVD-disc").hide();
    $(".Book").show();
  } else if(category.value == "Furniture"){
    $(".DVD-disc").hide();
    $(".Book").hide();
    $(".Furniture").show();
  } else if(category.value == "DVD-disc"){
    $(".DVD-disc").show();
    $(".Book").hide();
    $(".Furniture").hide();
  } else {
    $(".DVD-disc").hide();
    $(".Book").hide();
    $(".Furniture").hide();
  }
}

category.addEventListener("change",showFields);
