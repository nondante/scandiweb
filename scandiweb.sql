-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 24, 2018 at 02:23 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `SKU` varchar(10) DEFAULT NULL,
  `Name` varchar(30) DEFAULT NULL,
  `Price` varchar(30) DEFAULT NULL,
  `Type` varchar(30) DEFAULT NULL,
  `Size` varchar(30) DEFAULT NULL,
  `Weight` varchar(30) DEFAULT NULL,
  `Dimensions` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `SKU`, `Name`, `Price`, `Type`, `Size`, `Weight`, `Dimensions`) VALUES
(2, 'JVC200123', 'Acme DISC', '100$', 'DVD-disc', '700MB', NULL, NULL),
(3, 'JVC200124', 'LG DISC', '150$', 'DVD-disc', '800MB', NULL, NULL),
(4, 'JVC200125', 'SONY DISC', '150$', 'DVD-disc', '800MB', NULL, NULL),
(5, 'JVC200126', 'War and Peace', '50$', 'Book', NULL, '2kg', NULL),
(6, 'JVC200127', 'Aliens', '50$', 'Book', NULL, '2kg', NULL),
(7, 'JVC200128', 'Forest Gump', '60$', 'Book', NULL, '1kg', NULL),
(8, 'JVC200129', 'Chair', '60$', 'Furniture', NULL, NULL, '24x45x15'),
(9, 'JVC200130', 'Table', '100$', 'Furniture', NULL, NULL, '24x45x15'),
(10, 'JVC200131', 'Sofa', '160$', 'Furniture', NULL, NULL, '224x145x15'),
(13, 'GGWP0011', 'Table', '500$', 'Furniture', '', '', '100x200x50'),
(14, 'GGWP0222', 'Winter', '500', 'Book', '', '1kg', 'xx');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SKU` (`SKU`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
