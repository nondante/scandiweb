<?php
//A class for creating connection with the DataBase
class Dbconfig {
    protected $serverName;
    protected $userName;
    protected $passCode;
    protected $dbName;

    public function connect()
    {
	     $this->serverName ="localhost";
       $this->userName ="root";
       $this->passCode ="root";
       $this->dbName ="scandiweb";

       $conn = new mysqli($this->serverName,$this->userName,$this->passCode,$this->dbName);
       return $conn;
    }
  }

?>
