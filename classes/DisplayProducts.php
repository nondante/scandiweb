<?php
require_once('GetProducts.php');

  //A Class for displaying all items
  class DisplayProduct extends GetProduct
  {

    // Showing discs
    public function displayDiscs()
    {

      $discs = $this->getDiscs();
      foreach ($discs as $disc) {

        echo "<li class='product'><p>
           " . $disc["SKU"] . "</br>
           " . $disc["Name"] . "</br>
           " . $disc["Price"] . "</br>
           " . $disc["Size"] . "</br>
           </p></li>";
      }
    }
    // Showing books
    public function displayBooks()
    {
      $books = $this->getBooks();
      foreach ($books as $book) {

        echo "<li class='product'><p>
           " . $book["SKU"] . "</br>
           " . $book["Name"] . "</br>
           " . $book["Price"] . "</br>
           " . $book["Weight"] . "</br>
           </p></li>";
      }
    }

    // Showing furniture items
    public function displayFurniture()
    {

      $furniture = $this->getFurniture();
      foreach ($furniture as $furn) {

        echo "<li class='product'><p>
           " . $furn["SKU"] . "</br>
           " . $furn["Name"] . "</br>
           " . $furn["Price"] . "</br>
           " . $furn["Dimensions"] . "</br>
           </p></li>";
      }
    }
  }




 ?>
