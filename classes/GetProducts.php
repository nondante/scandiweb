<?php
require_once('Dbconfig.php');

//A Class for getting all items from the DataBase
 class GetProduct extends Dbconfig {

    //Getting discs
    public function getDiscs()
    {

      $sql = "SELECT SKU, Name, Price, Type, Size
             FROM products
             WHERE Type = 'DVD-disc'";

     $result = $this->connect()->query($sql);
     $numRows = $result->num_rows;

     if($numRows>0){
        while($row = $result->fetch_assoc()){
          $discs[] = $row;
        }
        return $discs;
      }
     }

     //Getting books
     public function getBooks()
     {

       $sql = "SELECT SKU, Name, Price, Type, Weight
              FROM products
              WHERE Type = 'Book'";

      $result = $this->connect()->query($sql);
      $numRows = $result->num_rows;

      if($numRows>0){
         while($row = $result->fetch_assoc()){
           $books[] = $row;
         }
         return $books;
       }
      }

      //Getting furniture items
      public function getFurniture()
      {

        $sql = "SELECT SKU, Name, Price, Type, Dimensions
               FROM products
               WHERE Type = 'Furniture'";

       $result = $this->connect()->query($sql);
       $numRows = $result->num_rows;

       if($numRows>0){
          while($row = $result->fetch_assoc()){
            $furniture[] = $row;
          }
          return $furniture;
        }
       }

    }

?>
