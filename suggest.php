
<?php

//Include files
include("connection.php");
include("inc/functions.php");

//Filtering inputs from the form
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $SKU = trim(filter_input(INPUT_POST,"SKU",FILTER_SANITIZE_STRING));
    $name = trim(filter_input(INPUT_POST,"name",FILTER_SANITIZE_STRING));
    $price = trim(filter_input(INPUT_POST,"price",FILTER_SANITIZE_STRING));
    $category = trim(filter_input(INPUT_POST,"category",FILTER_SANITIZE_STRING));
    $size = trim(filter_input(INPUT_POST,"size",FILTER_SANITIZE_STRING));
    $height = trim(filter_input(INPUT_POST,"height",FILTER_SANITIZE_STRING));
    $width = trim(filter_input(INPUT_POST,"width",FILTER_SANITIZE_STRING));
    $length = trim(filter_input(INPUT_POST,"length",FILTER_SANITIZE_STRING));
    $weight = trim(filter_input(INPUT_POST,"weight",FILTER_SANITIZE_STRING));

    //Validating mandatory fields
    if ($SKU == "" || $name == "" || $category == "" || $price == "") {
        $error_message = "Please fill in the required fields: SKU, Name, Price and Type";
    }
    if($category == "DVD-disc" && $size ==""){
      $error_message = "Please fill in the size of the disc";
    }
    if($category == "Book" && $weight ==""){
      $error_message = "Please fill in the weight of the book";
    }
    if($category == "Furniture" && ($height =="" || $width=="" || $length=="")){
      $error_message = "Please fill in the height, width and length";
    }
    //SKU must be unique
    $SKUs = validateSKU($SKU);
    foreach ($SKUs as $item) {
        if($item["SKU"] == $SKU){
          $error_message = "SKU must be unique.";
        }
    }



    if(!isset($error_message)){
      //Connect
      $db = mysqli_connect("localhost", "root", "root", "scandiweb") or die("Connection failed.");
      // Attempt insert query execution
      $sql = "INSERT INTO products (SKU, Name, Price, Type, Size, Weight, Dimensions)
              VALUES ('$SKU', '$name', '$price', '$category', '$size', '$weight', CONCAT('$height','x','$width','x','$length'))";
      if(mysqli_query($db, $sql)){
          echo "Records inserted successfully.";
      } else{
          echo "ERROR: Could not able to execute $sql. " . mysqli_error($db);
      }
    }

}
//Display error message
if (isset($error_message)) {
    echo "<div class='messageWrapper'><p class='message'>".$error_message . "</p></div>";
}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="reset.css" />
    <link rel="stylesheet" href="styles.css" />

    <title>ScandiWeb</title>
  </head>
  <body>
    <div class="wrapper">
      <h1>Product Add</h1>
      <span class="underline"></span>
      <div class="formContainer">
        <form method="post" action="suggest.php">
          <div class="table">
          <table>

            <tr>
                <th><label for="SKU">SKU</label></th>
                <td><input type="text" id="SKU" name="SKU" value="<?php if (isset($SKU)) { echo $SKU; } ?>" /></td>
            </tr>
            <tr>
                <th><label for="name">Name</label></th>
                <td><input type="text" id="name" name="name" value="<?php if (isset($name)) { echo $name; } ?>" /></td>
            </tr>
            <tr>
                <th><label for="price">Price</label></th>
                <td><input type="text" id="price" name="price" value="<?php if (isset($price)) { echo $price; } ?>" /></td>
            </tr>
            <tr>
                <th><label for="category">Type Switcher</label></th>
                <td><select id="category" name="category">
                    <option value="">Select One</option>
                    <option value="DVD-disc">DVD-disc</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                </select></td>
            </tr>
            <tr class="DVD-disc">
                <th><label for="size">Size</label></th>
                <td><input type="text" id="size" name="size" value="<?php if (isset($size)) { echo $size; } ?>" /><p></br>Please provide size of the disc.</p></td>
            </tr>
            <tr class="Furniture">
                <th><label for="height">Height</label></th>
                <td><input type="text" id="height" name="height" value="<?php if (isset($height)) { echo $height; } ?>" /></td>
            </tr>
            <tr class="Furniture">
                <th><label for="width">Width</label></th>
                <td><input type="text" id="width" name="width" value="<?php if (isset($width)) { echo $width; } ?>" /></td>
            </tr>
            <tr class="Furniture">
                <th><label for="length">Length</label></th>
                <td><input type="text" id="length" name="length" value="<?php if (isset($length)) { echo $length; } ?>" /><p></br>Please provide height,</br>width and length of the furniture item.</p></td>
            </tr>
            <tr class="Book">
                <th><label for="weight">Weight</label></th>
                <td><input type="text" id="weight" name="weight" value="<?php if (isset($weight)) { echo $weight; } ?>" /><p></br>Please provide weight of the book.</p></td>
            </tr>

          </table>
          </div>
          <input class="button" type="submit" value="Save" />
        </form>
      </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
